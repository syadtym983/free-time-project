from django.contrib import admin
from django.urls import path, include

from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/hr_department/', include("HR_department.urls")),
    path('api/login/', TokenObtainPairView.as_view()),
]
