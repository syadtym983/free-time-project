from django.db import models

from rest_framework import status
from django.contrib.admin import ModelAdmin


class BaseModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class BaseAdmin(ModelAdmin):
    readonly_fields = ["created", "updated"]


class BaseApiExceptions(Exception):
    status_code = status.HTTP_400_BAD_REQUEST
    message = ""
    message_fa = ""
