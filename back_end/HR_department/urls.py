from HR_department import views as HR_department_views

from django.urls import path

from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'invitation-requests', HR_department_views.InvitationRequestViewSet)
router.register(r'profiles', HR_department_views.ProfilesViewSet)
urlpatterns = router.urls

urlpatterns += [
    path("employee-register/", HR_department_views.EmployeeRegisterView.as_view()),
    path("employee-credentials/", HR_department_views.EmployeeCredentialsView.as_view())
]