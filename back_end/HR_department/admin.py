from django.contrib import admin

from utils import bases as utils_bases
from HR_department import models as HR_department_models


class EmployeeAdmin(utils_bases.ModelAdmin):
    list_display = [field.name for field in HR_department_models.Employee._meta.fields]


class ProfileAdmin(utils_bases.ModelAdmin):
    list_display = [field.name for field in HR_department_models.Profile._meta.fields]


class InvitationRequestAdmin(utils_bases.ModelAdmin):
    list_display = [field.name for field in HR_department_models.InvitationRequest._meta.fields]


admin.site.register(HR_department_models.Employee, EmployeeAdmin)
admin.site.register(HR_department_models.Profile, ProfileAdmin)
admin.site.register(HR_department_models.InvitationRequest, InvitationRequestAdmin)
