from rest_framework import viewsets, status
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny
from rest_framework.exceptions import NotFound
from rest_framework.views import APIView
from rest_framework.response import Response

from HR_department import models as HR_department_models
from HR_department import serializers as HR_department_serializers
from HR_department import permissions as HR_department_permissions


class EmployeeRegisterView(CreateAPIView):
    serializer_class = HR_department_serializers.ProfileSerializer
    permission_classes = [AllowAny]


class InvitationRequestViewSet(viewsets.ModelViewSet):
    queryset = HR_department_models.InvitationRequest.objects.all()
    serializer_class = HR_department_serializers.InvitationRequestSerializer
    permission_classes = [HR_department_permissions.HRManagerPermission]

    def get_serializer_context(self) -> dict:
        context = super().get_serializer_context()
        context.update({"request": self.request})
        return context


class ProfilesViewSet(viewsets.ModelViewSet):
    queryset = HR_department_models.Profile.objects.all()
    serializer_class = HR_department_serializers.ProfileSerializer

    def get_serializer_context(self) -> dict:
        context = super().get_serializer_context()
        context.update({"request": self.request})
        return context

    def get_permissions(self):
        if self.action == "retrieve":
            return [HR_department_permissions.EmployeePermission()]
        elif self.action == "list":
            return [HR_department_permissions.ViewEmployeesProfilesPermission()]
        elif self.action == "update":
            return [HR_department_permissions.ChangeViewEmployeesProfilesPermission()]
        else:
            Response(data={"error": "method not allowed"}, status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def get_queryset(self):
        if self.request.user.employee.position == HR_department_models.Employee.Position.Employee.value:
            return self.queryset.filter(employee=self.request.user.employee)
        else:
            return super(ProfilesViewSet, self).get_queryset()

    def create(self, request, *args, **kwargs):
        raise NotFound


class EmployeeCredentialsView(APIView):
    permission_classes = [HR_department_permissions.EmployeePermission]

    def get(self, request):
        data = {
            "username": request.user.username,
            "position": HR_department_models.Employee.Position(request.user.employee.position).name,
            "organization_email": request.user.employee.profile.organization_email,
            "profile_id": request.user.employee.profile.id
        }
        return Response(data=data, status=status.HTTP_200_OK)
