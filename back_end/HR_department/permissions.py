from rest_framework.permissions import BasePermission

from HR_department import models as HR_department_models


class EmployeePermission(BasePermission):
    def has_permission(self, request, view):
        return hasattr(request.user, "employee")


class ViewEmployeesProfilesPermission(EmployeePermission):
    def has_permission(self, request, view):
        return (super(ViewEmployeesProfilesPermission, self).has_permission(request, view)) and (
                request.user.employee.position in [HR_department_models.Employee.Position.HRManager.value,
                                                   HR_department_models.Employee.Position.SalaryManager.value])


class ChangeViewEmployeesProfilesPermission(ViewEmployeesProfilesPermission):
    pass


class HRManagerPermission(EmployeePermission):
    def has_permission(self, request, view):
        return super(HRManagerPermission, self).has_permission(request, view) and (
                request.user.employee.position == HR_department_models.Employee.Position.HRManager.value)
