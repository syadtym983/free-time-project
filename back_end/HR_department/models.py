from hashlib import sha256

from django.conf import settings
from django.db import models, transaction
from django.db.models import F
from django.contrib.auth import get_user_model
from django.core.validators import ValidationError

from utils import bases as utils_bases
from HR_department import exceptions as HR_department_exceptions
from HR_department import tasks as HR_department_tasks

User = get_user_model()


class Employee(utils_bases.BaseModel):
    class Position(models.IntegerChoices):
        Employee = 1
        HRManager = 2
        SalaryManager = 3

    user = models.OneToOneField(User, on_delete=models.PROTECT)
    position = models.SmallIntegerField(choices=Position.choices, default=Position.Employee.value)

    def __str__(self) -> str:
        return self.user.username

    def save(self, *args, **kwargs):
        if not self.pk:
            with transaction.atomic():
                super(Employee, self).save(*args, **kwargs)
                Profile.objects.create(
                    employee=self
                )
        else:
            super(Employee, self).save(*args, **kwargs)


class Profile(utils_bases.BaseModel):
    name = models.CharField(max_length=255)
    national_code = models.CharField(max_length=31, unique=True, null=True)
    birth_data = models.DateField(null=True)
    salary = models.DecimalField(max_digits=32, decimal_places=8, null=True, blank=True)
    organization_email = models.EmailField(blank=True)
    employee = models.OneToOneField(Employee, on_delete=models.PROTECT)

    def get_organization_email(self) -> str:
        return f"{self.employee.user.username}@mahdi.org"

    def save(self, *args, **kwargs):
        if not self.pk:
            self.organization_email = self.get_organization_email()
        super(Profile, self).save(*args, **kwargs)


class InvitationRequest(utils_bases.BaseModel):
    class InvitationState(models.IntegerChoices):
        Sent = 1
        Verified = 2

    hr_admin = models.ForeignKey(Employee, on_delete=models.PROTECT, related_name="sent_invitations")
    state = models.SmallIntegerField(choices=InvitationState.choices, default=InvitationState.Sent.value)
    newcomer_email = models.EmailField()
    employee = models.OneToOneField(Employee, on_delete=models.PROTECT, null=True, blank=True)
    short_link_param = models.CharField(max_length=255, blank=True)
    sent_times = models.PositiveIntegerField(default=1)

    def create_short_link_param(self) -> str:
        short_link_param = sha256(
            str(self.newcomer_email).encode('utf-8') + str(self.hr_admin.profile.organization_email).encode(
                "utf-8") + str(
                self.created).encode("utf-8")).hexdigest()
        return short_link_param

    def send_email(self) -> None:
        url_prefix = settings.SYSTEM_URL_PREFIX + "/register/"
        short_link = f"{url_prefix}/?short_link={self.short_link_param}"
        message = f"لینک دعوت شما : {short_link}"
        subject = "HR Invitation Request"
        HR_department_tasks.send_email_async.delay(subject, message, self.newcomer_email)

    def resend_invitation(self) -> None:
        if self.state == InvitationRequest.InvitationState.Sent.value:
            self.sent_times = F("sent_times") + 1
            self.save(update_fields=['sent_times', 'updated'])
            self.send_email()
        else:
            raise HR_department_exceptions.InvitationRequestAlreadyVerified

    def clean(self):
        if self.hr_admin.position != Employee.Position.HRManager.value:
            raise ValidationError("only Hr HR Manage can invite employee")

    def save(self, *args, **kwargs):
        if not self.pk:
            self.clean()
            self.short_link_param = self.create_short_link_param()
            self.send_email()
        super(InvitationRequest, self).save(*args, **kwargs)
