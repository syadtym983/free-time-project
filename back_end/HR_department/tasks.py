from django.core.mail import EmailMessage

from celery import shared_task


@shared_task()
def send_email_async(subject: str, message: str, to: str) -> None:
    email = EmailMessage(subject, message, to=[to])
    email.send()
