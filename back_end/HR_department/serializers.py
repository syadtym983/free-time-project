from django.contrib.auth import get_user_model
from django.conf import settings
from django.contrib.auth.hashers import make_password
from django.contrib.auth.password_validation import CommonPasswordValidator, NumericPasswordValidator, \
    UserAttributeSimilarityValidator, MinimumLengthValidator

from rest_framework import serializers
from rest_framework import validators

from HR_department import models as HR_department_models

User = get_user_model()


class InvitationRequestSerializer(serializers.ModelSerializer):
    state = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = HR_department_models.InvitationRequest
        fields = ["hr_admin", "state", "newcomer_email", "short_link_param", "sent_times"]
        read_only_fields = ["hr_admin", "short_link_param", "sent_times", "state"]

    def get_state(self, obj):
        return HR_department_models.InvitationRequest.InvitationState(obj.state).name

    def create(self, validated_data):
        validated_data["hr_admin"] = self.context['request'].user.employee.hrmanager
        return super(InvitationRequestSerializer, self).create(validated_data)


class ProfileSerializer(serializers.ModelSerializer):
    username = serializers.SerializerMethodField()
    password = serializers.CharField(write_only=True, required=True,
                                     validators=[CommonPasswordValidator, NumericPasswordValidator,
                                                 UserAttributeSimilarityValidator, MinimumLengthValidator])

    class Meta:
        model = HR_department_models.Profile
        fields = ["national_code", "birth_data", "salary", "username", "password", "name", "organization_email"]

    def get_username(self, obj):
        return obj.employee.user.username

    def validate_username(self, value):
        if User.objects.filter(username=value).exist():
            raise validators.ValidationError(detail="user name already exist")

    def validate_national_code(self, value):
        if HR_department_models.Profile.objects.filter(national_code=value).exists():
            raise validators.ValidationError(detail="national code already exist")

    def update(self, instance, validated_data):
        employee = self.context['request'].user.employee
        if validated_data.get("password") is not None:
            validated_data.pop("password")
        if employee.position == HR_department_models.Employee.Position.HRManager.value:
            if validated_data.get("salary") is not None:
                validated_data.pop("salary")
        elif employee.position == HR_department_models.Employee.Position.SalaryManager.value:
            validated_data = {"salary": validated_data.get("salary")}

        return super(ProfileSerializer, self).update(instance, validated_data)


class RegisterEmployeeSerializer(serializers.Serializer):
    short_link_param = serializers.CharField(required=True)
    profile = ProfileSerializer()

    def validate_short_link(self, value):
        try:
            HR_department_models.InvitationRequest.objects.get(short_link_param=value,
                                                               state=HR_department_models.InvitationRequest.InvitationState.Sent.value)
        except HR_department_models.InvitationRequest.DoesNotExist:
            raise validators.ValidationError(detail="short link is not valid")

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data["username"],
            password=make_password(validated_data["password"])
        )
        profile = HR_department_models.Profile.objects.create(
            name=validated_data["name"],
            national_code=validated_data["national_code"],
            birth_data=validated_data["birth_data"],
            salary=validated_data["salary"]
        )
        employee = HR_department_models.Employee.objects.create(
            user=user,
            profile=profile
        )
        return employee

    def update(self, instance, validated_data):
        raise NotImplementedError
