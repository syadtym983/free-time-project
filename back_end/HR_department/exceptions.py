from utils import bases as utils_bases


class InvitationRequestAlreadyVerified(utils_bases.BaseApiExceptions):
    message = "invitation request is verified"
    message_fa = "یا این دعوت نامه ثبت نام صورت گرفته شده است"